﻿using System;

namespace Ejemplo07_Strategy_Abstract_Class
{
    class MainApp
    {
        static void Main()
        {
            Context context;
            // StrategyBase s = new StrategyBase("mensaje");
            // No podemos instanciar clases abstractas

            // Tres contextos con diferentes estrategias
            context = new Context(new ConcreteStrategyA("Eeeeeeooh "));
            context.Execute();

            context = new Context(new ConcreteStrategyB());
            context.Execute();

            context = new Context(new ConcreteStrategyC("Nombre directo"));
            context.Execute();

        }
        //Una clase abstracta lo principal es que no puede ser instanciada como las interfaces
        //puede tener algunos métodos implementados y otros sin implementar 
        //(declarando sólo su interfaz). Estos métodos son abstractos
        static class ConcretesStrategies
        {
            public static StrategyBase AddConcreteStrategyA(StrategyBase strObj)
            {
                //DInámicamente de damos la funcionalidad qeu antes hacíamos por herencia
                //Tanto public override void Execute() como MostrarNombre()
                

               
                return strObj;
            }
        }
        

        // Implementa el algoritmo usando el patron estrategia
        class ConcreteStrategyA : StrategyBase
        {
            public ConcreteStrategyA(string mensaje): base(mensaje)
            {
                
            }
            public override void Execute()
            {
                RepetirChar('-', 30);
                MostrarNombre();
            }
            public override void MostrarNombre()
            {
                RepetirChar('_', 30);
                base.MostrarNombre();
                RepetirChar('_', 30);
            }
        }

        class ConcreteStrategyB : StrategyBase
        {
            public ConcreteStrategyB() : base("Llamar a")
            {
            }

            public override void Execute()
            {
                MostrarNombre();
                RepetirChar('\n', 3);
            }
        }

        class ConcreteStrategyC : StrategyBase
        {
            public ConcreteStrategyC(string nombre) : base("")
            {
                this.nombre = nombre;
            }

            public override void Execute()
            {
                RepetirChar('+', 30);
                Console.WriteLine("Called ConcreteStrategyC.Execute()");
                Console.WriteLine(nombre);
            }
        }
        // Contiene un objeto ConcreteStrategy y mantiene una referencia a un objeto Strategy
        class Context
        {
            StrategyBase strategy;

            // Constructor
            public Context(StrategyBase strategy)
            {
                this.strategy = strategy;
            }

            public void Execute()
            {
                strategy.Execute();
            }
        }
    }
}