﻿using System;
using System.Collections.Generic;
using System.Text;

//No podemos usar nada de Ejercicio08_StrategyLambdas
//Ni de program ni  me main.

//Ya no se puede heredar de esta clase
//sino sobreescribir las funcionalidades mediante delegados(variables de función,
//funciones estáticas y/o lambdas)

namespace Ejemplo07_Strategy_Abstract_Class
{
    class StrategyBase
    {
        protected string nombre;
        public StrategyBase(string mensaje)
        {
            nombre = mensaje + " " + GetType().Name + ".Excecute()";
        }
        protected void RepetirChar(char caracter, int veces)
        {
            for (int i = 0; i < veces; i++)
            {
                Console.Write(caracter);
            }
            Console.WriteLine("\n");
        }
        public abstract void Execute();
        public virtual void MostrarNombre()
        {
            Console.WriteLine(nombre);
        }
    }
}
