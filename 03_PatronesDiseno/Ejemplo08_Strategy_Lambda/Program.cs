﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ejemplo07_Strategy_Abstract_Class
{
    class Program
    {
        
        static void Main(string[] args)
        {
            var button1 = new MyButton((colecc) => colecc.Sum().ToString(), "Add 'em");
            var button2 = new MyButton((colecc) => string.Join(" ", colecc.Select(num => num.ToString()).ToArray()), "Join 'em");
            var numbers = Enumerable.Range(1, 10);
            Console.WriteLine(button1.Submit((IEnumerable<int>)numbers));
            Console.WriteLine(button2.Submit((IEnumerable<int>)numbers));
            Console.ReadLine();
        }    
    }

    public class MyButton
    {
          private readonly Func<IEnumerable<int>, string> submitFunction;
          public string Label { get; private set; }

          public MyButton(Func<IEnumerable<int>, string> submitFunction, string label)
          {
              this.submitFunction = submitFunction;
              Label = label;
          }

          public string Submit(IEnumerable<int> data)
          {
              return submitFunction(data);
          }
    }
}
namespace Otro
    

