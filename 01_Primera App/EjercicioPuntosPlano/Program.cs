﻿using System;

namespace EjercicioPuntosPlano
{
    class Program
    {
        static void Main(string[] args)
        {
            int numPuntos,contador, x,y, cuadranteI=0,cuadranteII=0,cuadranteIII=0,cuadranteIV=0;
            int[] cont = new int[4]; //array es de valor fijo y de un solo tipo
            Console.WriteLine("¿Cuántos puntos va a ingresar?");
            numPuntos = int.Parse(Console.ReadLine()) ;
            for (contador = 0; contador < numPuntos; contador++)
            {
                Console.Write("Ingrese punto x");
                x = int.Parse(Console.ReadLine());
                Console.Write("Ingrese punto y");
                y = int.Parse(Console.ReadLine());
                /*if (x >= 0 && y >= 0)
                {
                    cuadranteI++;
                }
                else
                {
                    if (x < 0 && y > 0)
                    {
                        cuadranteII++;
                    }
                    else
                    {
                        if(x<0 &&y<0)
                        {
                            cuadranteIII++;
                        }
                        else
                        {
                            cuadranteIV++;   
                        }
                    }
                }*/
                //Con operador ternario
                cuadranteI+=(x > 0 && y > 0) ? 1 : 0;
                cuadranteII+=(x > 0 && y > 0) ? 1 : 0;
                cuadranteIII+=(x > 0 && y > 0) ? 1 : 0;
                cuadranteIV+=(x > 0 && y > 0) ? 1 : 0;


            }
            Console.WriteLine("Existen " + cuadranteI + " en el cuadrante uno"); 
            Console.WriteLine("Existen " + cuadranteII + " en el cuadrante dos");
            Console.WriteLine("Existen " + cuadranteIII + " en el cuadrante tres");
            Console.WriteLine("Existen " + cuadranteIV + " en el cuadrante cuatro");   

        }
    }
}
