﻿            /*
            * Crear una estuctura usuario con nombre (texto), edad (entero), y altura (decimal)
            * Crear dos variables usuarios
            * Crear un array de usuarios con 4 usuarios, los dos de antes, y dos más que pidamos 
            * por teclado.
            * Crear un método MostrarUsuario que muestre sus datos al terminar, mostrar los 4 usuarios.
            * */
using System;

namespace Ejercicio13_Estructura
{
    class Program
    {
        static Usuario[] usuariosArray;

        static void Main(string[] args)
        {

            Program.usuariosArray = new Usuario[4];
            string nombre;
            int edad;
            float altura;
         
            Usuario usu1, usu2,usu3,usu4;

            usu1 = new Usuario("Leo",30,1.8f);
            usu2 = new Usuario("Dana", 12, 1.75f);
            usu1.MostrarUsuario();

            Console.Write("Ingrese el nombre del tercer usuario: ");
            nombre = Console.ReadLine();
            Console.Write("Ingrese la edad del tercer usuario: ");
            edad = int.Parse(Console.ReadLine());
            Console.Write("Ingrese la altura del tercer usuario: ");
            altura = float.Parse(Console.ReadLine());
            usu3 = new Usuario(nombre,edad,altura);
            Console.Write("Ingrese el nombre del cuarto usuario: ");
            nombre = Console.ReadLine();
            Console.Write("Ingrese la edad del cuarto usuario: ");
            edad = int.Parse(Console.ReadLine());
            Console.Write("Ingrese la altura del cuarto usuario: ");
            altura = float.Parse(Console.ReadLine());
            usu4 = new Usuario(nombre, edad, altura);

            usuariosArray[0] = usu1;
            usuariosArray[1] = usu2;
            usuariosArray[2] = usu3;
            usuariosArray[3] = usu4;

            MostrarUsuarios();
        }

        private static void MostrarUsuarios()
        {
            for (int i = 0; i < 3; i++)
            {
                MostrarUsuario(Program.usuariosArray[i]);
            }
        }

        private static void MostrarUsuario(Usuario usuario)
        {
            Console.WriteLine("Nom: " + usuario.nombre + ", Edad: " + usuario.edad);
        }


    }
}

public struct Usuario
{
    public string nombre;
    public int edad;
    public float altura;
    public Usuario(string nombre, int edad, float altura)
    {
        this.nombre = nombre;
        this.edad = edad;
        this.altura = altura;
    }

    public void MostrarUsuario()
    {
        Console.WriteLine("Nom: " + nombre + ", Edad: " + edad);
    }
}

