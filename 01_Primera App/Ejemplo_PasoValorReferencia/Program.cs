﻿using System;

namespace Ejemplo_PasoValorReferencia
{
    class Program
    {
        
        static void Main(string[] args)
        {
            int variableEnt = 10;
            

            Console.WriteLine("Entero fuera y antes: " + variableEnt);
            RecibimosUnValor(variableEnt);
            Console.WriteLine("Entero fuera y después: " + variableEnt);

            string nombre = "Marco";
           
            Console.WriteLine(RecibeString(ref nombre));

        }
        static void RecibimosUnValor(int entero)
        {
            Console.WriteLine("Entero dentro y antes: " + entero);
            entero = entero * 2 + 2;
            Console.WriteLine("Entero dentro y después: "+ entero);
        }

        static void RecibimosUnaRef(ref int entero)
        {
            Console.WriteLine("Entero dentro y antes: " + entero);
            entero = entero * 2 + 1;
            Console.WriteLine("Entero dentro y después: " + entero);
        }

        static string RecibeString(ref string nombre) 
        {
            nombre = nombre.ToUpper(); // aquí si que se cambia el valor 
            return nombre;
        }
    }
}
