﻿using System;

namespace OperadorDoblePregunta
{
    class Program
    {
        static void Main(string[] args)
        {

            //OPERADOR TERNARIO
            int condicion, x=5, y=1;
            string linea1 = "es verdadero";
            string linea2 = "es falso";
            string resultado;
            resultado = (x > y) ? linea1 : linea2; // linea1 y 2 puede ser cualquier cosa
            Console.Write(resultado);

            //OPERADOR ??

            int ? a = 8;
            int b = a ?? -1; 
            Console.WriteLine(b); // devuelve el valor 8

            string entero = null;
            Console.WriteLine(entero ?? "cadena" );// si es null se evalua la parte derecha, en el caso que no sea null, devuelve ese valor
            //y no evalua la parte derecha

        }
    }
}
