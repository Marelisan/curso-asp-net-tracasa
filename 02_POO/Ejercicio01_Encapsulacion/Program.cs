﻿using System;

namespace Ejercicio01_Encapsulacion
{
    class Program
    {
        static void Main(string[] args)
        {

            Usuario u1 = new Usuario("Usuario Guay", 20, 3.5f);
            MostrarUsuario(u1);
            Usuario u2 = new Usuario("", 20, 3.5f);
            MostrarUsuario(u2);
            Usuario u3 = new Usuario("Usuario Guay", -4, 3.5F);
            MostrarUsuario(u3);
            Usuario u4 = new Usuario(null, 20, 0.003F);
            MostrarUsuario(u4);
        }
        public static void MostrarUsuario(Usuario usuario)
        {

            Console.WriteLine("Nombre: " + usuario.Nombre);
            Console.WriteLine("Edad:   " + usuario.Edad);
            Console.WriteLine("Altura:   " + usuario.Altura);
        }
    }
}
