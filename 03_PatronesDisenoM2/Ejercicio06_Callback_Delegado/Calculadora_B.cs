﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo06_FuncionCallback
{
    public static class Calculadora_B
    {
        static float resultado = 0;

             static float SumarB(float[] datos, string operador) // se puede poner antes del public el static
            {

                for (int i = 0; i < datos.Length; i++)
                {
                    if (i == 0)
                    {
                        resultado = datos[0];
                    }
                    else
                    {
                        resultado = resultado + datos[i];
                    }
                }
            return resultado;
            }
            static float RestaArray(float[] datos)
            {
                for (int i = 0; i < datos.Length; i++)
                {
                    if (i == 0)
                    {
                        resultado = datos[0];
                    }
                    else
                    {
                        resultado = resultado - datos[i];
                    }
                }
            return resultado;
        }

            static float MultArray(float[] datos)
            {
                for (int i = 0; i < datos.Length; i++)
                {
                    if (i == 0)
                    {
                        resultado = datos[0];
                    }
                    else
                    {
                        resultado = resultado * datos[i];
                    }
                }
            return resultado;
        }

            static float DivArray(float[] datos)
            {
                for (int i = 0; i < datos.Length; i++)
                {
                    if (i == 0)
                    {
                        resultado = datos[0];
                    }
                    else //Se analiza desde el segundo elemento
                    {
                        if (i != 0)
                        {
                            resultado = resultado / datos[i];
                        }
                    }
                }
            return resultado;
            }
        
        
    }
}
