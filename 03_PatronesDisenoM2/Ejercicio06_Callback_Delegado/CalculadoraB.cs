﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio06_Callback_Delegado
{
    public static class CalculadoraB
    {
        static float resultado = 0;

        public static float Sumar_B(float[] datos, string operador) // se puede poner antes del public el static
        {

            for (int i = 0; i < datos.Length; i++)
            {
                if (i == 0)
                {
                    resultado = datos[0];
                }
                else
                {
                    resultado = resultado + datos[i];
                }
            }
            return resultado;
        }
        public static float RestaArray(float[] datos)
        {
            for (int i = 0; i < datos.Length; i++)
            {
                if (i == 0)
                {
                    resultado = datos[0];
                }
                else
                {
                    resultado = resultado - datos[i];
                }
            }
            return resultado;
        }

        public static float MultArray(float[] datos)
        {
            for (int i = 0; i < datos.Length; i++)
            {
                if (i == 0)
                {
                    resultado = datos[0];
                }
                else
                {
                    resultado = resultado * datos[i];
                }
            }
            return resultado;
        }

        public static float DivArray(float[] datos)
        {
            for (int i = 0; i < datos.Length; i++)
            {
                if (i == 0)
                {
                    resultado = datos[0];
                }
                else //Se analiza desde el segundo elemento
                {
                    if (i != 0)
                    {
                        resultado = resultado / datos[i];
                    }
                }
            }
            return resultado;
        }
    }
}
