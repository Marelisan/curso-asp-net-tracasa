﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio06_Callback_Delegado
{
    public static class VistaCalc
    {
        
        public static void PedirArray(FuncionDatos funcionDatos)
        {
            int contador = 0, controlador=0;
            string respuesta2;
            bool avisa = true;
            do
            {
                controlador = 1;
                Console.WriteLine("¿Cúantos operandos habrá?");
                if (int.TryParse(Console.ReadLine(), out int numOperandos))
                {
                    float[] nuevoArray = new float[numOperandos];
                    do
                    {
                        Console.WriteLine("Ingrese el número " + (contador+1) + " a operar");
                        if(float.TryParse(Console.ReadLine(),out float x))
                        {
                            nuevoArray[contador] = x;
                            contador++;
                        }
                        else
                        {
                            Console.WriteLine("Ingrese un número");
                        }  
                    }
                    while (contador < numOperandos);
                    do
                    {
                        Console.WriteLine("¿Qué operación va a realizar (+,-,*./)?");
                        respuesta2 = Console.ReadLine();
                        if((respuesta2 != "+") && (respuesta2 != "-") && (respuesta2 != "*") && (respuesta2 != "/"))
                        {
                            Console.WriteLine("Ingrese uno de los siguientes operadores (+,-,*./)?");
                            avisa = false;
                        }
                        else
                        {
                            avisa = true;
                        }
                    } while (avisa == false) ;

                    float resultado = funcionDatos(nuevoArray , respuesta2);// llamo a la función y me olvido de las operaciones
                    Console.WriteLine("El resultado de la " + respuesta2 + " es: "+resultado);   
                }
                else
                {
                    Console.WriteLine("Ingrese un número");
                    controlador = 0;
                }
            } while (controlador == 0);
            

            


            
        }

        
    }
}
