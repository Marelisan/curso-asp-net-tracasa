﻿using System;
using System.Collections.Generic;
// Ejercicio: Crear dos sistemas (clases independientes 
// que no se conocen entre sí): 
// La primera, CalculaArrays, tendrá 4 funciones para sumar, rest, mult, div
// todo con float. Estas podrán realizar la operación sobre un array:
// Ej: {9, 7, 5, 3}  suma -> 24     resta ->  -6        mul -> 945
//     {42, 3, 2}   div -> (42 / 3) / 2 -> 7        suma -> 47
//      {3} ->   cualquier operador devuelve 3.
//      {66} ->   cualquier operador devuelve 66.
// No puede estar vacio el array
// La segunda clase, VistaCalc, le pide al usuario cuantos operandos habrá,
//  y los operandos uno a uno, la operación, y mostrará el resultado
// Si alguien quiere, en vez de eso, que resuelva: 3+22++11 ó 7*3*11*2
namespace Ejercicio06_Callback_Delegado
{

    public delegate float FuncionDatos(float [] operandos, string operador);
    //public delegate string FuncionOperando(string operando);
    class Program
    {
        public static void Main(string[] args)
        {
            FuncionDatos funcionDatos = CalculaArrays.CalculaArray; //Instancio el delegado, este delegado va a hacer referencia al método PedirArray de VistaCalc 
            VistaCalc.PedirArray(funcionDatos);
            //VistaCalc.PedirArray(CalculaArrays.CalculaArray);
            


            
        }
    }
    }

