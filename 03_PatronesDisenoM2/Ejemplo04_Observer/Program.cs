﻿using System;

namespace Ejemplo04_Observer
{
    class Program
    {
        static void Main(string[] args)
        {
            MovidasObserver();
            //Invocar al Garbage Collector (recolector de basura)
            //busca objetos sin referencias, que no haya variables que apunte a ellos
            // y los elimina, libera su memoria de la RAM.
            System.GC.Collect();
            static void MovidasObserver()
            {
                Console.WriteLine("Patron observer: Periodico");
                PeriodicoObservado alDia = new PeriodicoObservado();

                // 1 - Un suscriptor humano
                SuscriptorHumano juan = new SuscriptorHumano("Juan");
                alDia.AddSuscriptor(juan);
                // 2 - Ocurre una noticia
                alDia.NotificarNoticia("Desubierto nuevo elemento químico", DateTime.Now);
                // 3 - Otro suscriptor humano
                SuscriptorHumano teo = new SuscriptorHumano("Teo");
                alDia.AddSuscriptor(teo);
                // 4 - Otro suscriptor máquina lleva un código int en vez de nombre
                // System.IO.AppendAllText("fich_maquina.txt", "Ejemplo texto")
                //System.IO.WritedAllText("fich_maquina.txt", "Ejemplo texto")
                SuscriptorMaquina maquina = new SuscriptorMaquina(131315);
                alDia.AddSuscriptor(maquina);

                // 5 - Ocurre otra noticia
                alDia.NotificarNoticia("Terminan con la inflación en Venezuela", DateTime.Now);
                //Noticia corazon:
                alDia.NotificarNoticiaCorazon("Actriz dona todo su dinero a causas benéficas", DateTime.Now);
                // 6 - El otro humano se desuscribe porque dice mucha mentira
                //     En el periodico tiene que haber un método para quitar suscriptores
                alDia.BajaSuscriptor(maquina);
                System.Threading.Thread.Sleep(2000);
                // 7 - Ocurre la última noticia
                alDia.NotificarNoticia("Ultima noticia: cerramos el periodico", DateTime.Now);
                // 8 - El periodico cierra
                alDia = null;
                // 9 - Que los suscriptores reciban también la fecha y hora de la noticia y  la muestren.

                //Para que el suscriptor de ciencia se pueda suscribir, simplemente se
                //asigna la función al campo delegado:
                alDia.NuevaNotCiencia = NationalGeo;
                //Cuando realmente ocurre la noticia de ciencia:
                alDia.NoticiaCiencia("EL meteorito existía y era de Kriptonita");
            }
            //Aqui las variables locales (primitivas, structs o referencias a objetos)se destruyen, 
            //Pero no los objetos en sí, esos quedan vivos en alguna parte
            
            //Este suscriptor sólo es una función
            static void NationalGeo(string noticiaDePseudociencia)
            {
                Console.WriteLine(">>>> A sabr: " + noticiaDePseudociencia);
            }
        }

    }
}

