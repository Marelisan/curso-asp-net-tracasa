﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo04_Observer
{
    delegate void NoticiaCiencia(string not); //aquí definimos la esctructira de la funcion
    class PeriodicoObservado
    {
        List<ISuscriptorObservador> listaSuscriptores;
        public NoticiaCiencia NuevaNotCiencia;
        public PeriodicoObservado()
        {
            listaSuscriptores = new List<ISuscriptorObservador>();
        }

        public void NoticiaCiencia(string laNoticia)
        {
            if(NuevaNotCiencia != null)
            {
                NuevaNotCiencia("Ciencia: " + laNoticia);
            }
        }



        public void NotificarNoticia(string titular, DateTime fecha)
        {
            Console.WriteLine("Última noticia");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(fecha.ToString() + " - " + titular);
            Console.ForegroundColor = ConsoleColor.White;
            foreach (var suscriptor in listaSuscriptores)
            {
                suscriptor.ActualizarNotificacionNoticia(titular, fecha);
            }
        }

        public void NotificarNoticiaCorazon(string titular, DateTime da)
        {
            foreach (var suscriptor in listaSuscriptores)
            {
                suscriptor.ActualizarNoticiaCorazon(titular, da);
            }
        }

        public void AddSuscriptor(ISuscriptorObservador observador)
        {
            listaSuscriptores.Add(observador);
        }

        public void BajaSuscriptor(ISuscriptorObservador observador)
        {
                    listaSuscriptores.Remove(observador);       
        }
        ~PeriodicoObservado()
        {
            NotificarNoticia("Cerramos el periodico", new DateTime(2021,08,21,11,00,00));
            listaSuscriptores.Clear();
        }
    }
}

