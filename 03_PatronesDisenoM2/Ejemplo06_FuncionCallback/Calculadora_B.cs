﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo06_FuncionCallback
{
    public static class Calculadora_B
    {
        public static float SumarB(float a1, float a2) // se puede poner antes del public el static
        {
            return a1 + a2;
        }

        public static float MultiplicarB(float a1, float a2)
        {
            float res = a1 * a2;
            return res;
        }
    }
}
