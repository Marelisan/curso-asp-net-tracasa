﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo06_FuncionCallback
{
    public static class Calculadora_A  //no se puede instanciar, no se puede crear objetos de la clase
    {
        public static float SumarA(float a1, float a2) // se puede poner antes del public el static
        {
            return a1 +  a2;
        }

        public static float MultiplicarA(float a1, float a2)
        {
            float res = a1 * a2;
            /*for(int i = 0; i < a2; i++)
            {
                res = ; 
            }*/
            return res;
        }
    }
}
