﻿using System;

namespace Ejemplo06_FuncionCallback
{
    //Creamos un nuevo tipo de dato, que indica que es una función
    //(ni clase, ni interfaz...). Donde lo que importa son los tipos de datos que 
    //recibe y que devuelve
    //Es decir, declaramos un delegado

    delegate float FuncionOperador(float op1, float op2);
    //delegate string FuncionOperador(string operador);

    //Ahora podremos crear variables de tipo función (float, float)
    class Program
    {
        static void Main(string[] args)
        {

          
            Console.WriteLine("Invocamos con suma");
            //Al pasar como parámetro la función SumarA, no lleva paréntesis, porque no se llama ahora, sino después
            VistaCalculadora(Calculadora_B.SumarB);

            Console.WriteLine("Invocamos con multiplicación");
            //Al pasar como parámetro la función SumarA, no lleva paréntesis, porque no se llama ahora, sino después
            VistaCalculadora(Calculadora_B.MultiplicarB);
            
        }

        //no sabe que operaciones realizar para eso necesita una funcion callback
        // se puede pasar por delegado 
        static void VistaCalculadora(FuncionOperador operador )
        {
            Console.WriteLine("Operando 1:");
            float x = float.Parse(Console.ReadLine());
            Console.WriteLine("Operando 2:");
            float y = float.Parse(Console.ReadLine());
            float resultado = operador(x, y);
            Console.WriteLine("Resultado: " + resultado);
        }

        /*static void VistaOperadora(FuncionOperador operador,)
        {
            string[] delimitadores = {"+","*"};
            do
            {
                Console.WriteLine("Ingrese los dos números con el operando de suma o multiplicacion: ");
                string operacion = Console.ReadLine();
                int posOp = operacion.IndexOf("+");
                if (posOp < 0)
                {
                    posOp = operacion.IndexOf("*");
                    if (posOp < 0)
                    {
                        Console.WriteLine("No te entiendo, humano");
                        return;
                    }
                }
                float x = float.Parse(operacion.Substring(0, posOp));
                float y = float.Parse(operacion.Substring(posOp+1));

                float resultado = operacion(x, y);
                Console.WriteLine("Resultado: " + resultado);
            }while()
        }*/

        //Ejercicio1: usar VistaCalculadora con una calc creada por vosotros, pero exacta. Llamadla
        //CalculadoraB.
        //Ejercicio2: 

    }
}
