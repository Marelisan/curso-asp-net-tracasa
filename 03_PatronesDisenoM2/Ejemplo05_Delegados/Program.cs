﻿using System;

namespace Ejemplo05_Lambdas
{
    //Vamos a crear un nuevo tipo de dato, que en vez de almacenar
    //información que almacene una función con determinada estructura
    // o firma.
    //las estructura en como la interfaz de una función, pero sin importar 
    //e nombre: Los tipos de datos que recibe, y el tipo de dato que devuelve
    //En C eran punteros a funciones, en Java hasta Java8,
    //Eran interfaces con una sóla funcion, en JS funciones fecha.
    //En c# son los delegados

    delegate void FuncionQueRecibeInt(int param);//Obligado el tipo de dato que recibe int y el 
    //que devuelve void.
    class Program
    {
        static void Main(string[] args)
        {
            // LAMBDAS
            // Invocar Funciones estáticas:
            FuncionEstatica(5);
            OtraEstatica(7);
            //Variables de tipos funcion (delegados)
            FuncionQueRecibeInt funRecint;
            funRecint = FuncionEstatica; //deve tener el mismo tipo de parámetro
            funRecint(200);
            Console.WriteLine("\n\n Otra librería, otr omódulo, otra función, hace:");
            OtroSistema(OtraEstatica);
            OtroSistema(FuncionEstatica);
            OtroSistema(funRecint);
            //funRecint = nul; provocaría una excepción
            funRecint = OtraEstatica;
        }
        static void OtroSistema(FuncionQueRecibeInt funExt)
        {
            //Queremos recibir una funcion como parámetro:
            //LlamarFuncionExterna por ejemplo, que reciba un int
            int valor = 3;
            Console.WriteLine("Otro sistema hace sus cosas");
            System.Threading.Thread.Sleep(2000);
            Console.WriteLine("Tardar su tiempo");
            Console.WriteLine("Y llamar a la funcionalidad externa");
            funExt(3);
            //LlamarFuncionExterna(3);
        }
        static void FuncionEstatica(int x)
        {
            Console.WriteLine("No necesito un objeto para ser llamada");
            Console.WriteLine("Param: "+ x);
        }
        
        static void OtraEstatica(int y)
        {
            Console.WriteLine(y + " - Otra estática");
        }
    }
}
