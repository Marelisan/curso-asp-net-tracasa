﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ModeloUsuarios
{
    class GestionUsuarios
    {
        int posicion;
        static GestionUsuarios instancia;

        List<Usuario> usuariosArray = new List<Usuario>();
        public static GestionUsuarios Instancia 
        {
            get
            {
                if (instancia == null)
                    instancia = new GestionUsuarios();

                return instancia;
            }
        }
        public void MostrarUno()
        {
            Console.WriteLine("Ingrese la posición de usuario a mostrar: ");
            posicion = int.Parse(Console.ReadLine());
            
            if (ComprobarArray(posicion) == true )
                usuariosArray[posicion].MostrarDatos();
            else
                Console.WriteLine("No existe usuario");
        }

        public bool ComprobarArray(int posicion)
        {

            if (usuariosArray.Count >= posicion)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void EliminarUno()
        {
           Console.WriteLine("Ingrese la posición de usuario a eliminar: ");
            posicion = int.Parse(Console.ReadLine());
            if (ComprobarArray(posicion) == true)
                usuariosArray.RemoveAt(posicion);
            else
                Console.WriteLine("No existe usuario");

        }

        public void CrearUno()
        {
            Usuario usu = new Usuario();
            usu.PedirDatos();
            usuariosArray.Add(usu);

        }        
        public void MostrarTodos()
        {
            Console.WriteLine("Usuarios: ");
            if (usuariosArray.Count != 0 )
                foreach (Usuario usuario in usuariosArray) 
                {
                    usuario.MostrarDatos();
                }
            else
                Console.WriteLine("No existe usuarios");

        }

        public void ModificarUno()
        {
            bool var;
            do
            {
                Console.WriteLine("Elige el usuario a modificar: ");
                var = int.TryParse(Console.ReadLine(), out posicion);
                
            } while (!var);
            if (ComprobarArray(posicion) == true)
                usuariosArray[posicion].PedirDatos();
            else
                Console.WriteLine("No existe usuario");
        }
    }
}
