﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ModeloUsuarios
{
    class VistaUsuario
    {
        Controlador controlador;
        int posicion;
        public VistaUsuario(Controlador controlador)
        {
            this.controlador = controlador;
        }

        public void Menu()
        {
            bool var = false;
            while (!var)
            {
                Console.WriteLine("MENU");
                Console.WriteLine("1.- Mostrar uno");
                Console.WriteLine("2.- Mostrar todo");
                Console.WriteLine("3.- Eliminar uno");
                Console.WriteLine("4.- Crear uno");
                Console.WriteLine("5.- Modificar uno");
                Console.WriteLine("6.- Salir");
                bool comprobar = int.TryParse(Console.ReadLine(), out int opcion);
                if (comprobar && (opcion >= 1 && opcion <= 6))
                {
                    switch (opcion)
                    {
                        case 1:
                            MostrarUno();
                            break;
                        case 2:
                            gesUsu.MostrarTodos();
                            break;
                        case 3:
                            gesUsu.EliminarUno();
                            break;
                        case 4:
                            gesUsu.CrearUno();
                            break;
                        case 5:
                            gesUsu.ModificarUno();
                            break;
                        case 6:
                            var = true;
                            break;
                    }
                }
                else
                    Console.WriteLine("Introduce un número válido");

            }
        }

        public void MostrarUno()
        {
            Console.WriteLine("Ingrese la posición de usuario a mostrar: ");
            posicion = int.Parse(Console.ReadLine());

            if (ComprobarArray(posicion) == true)
                usuariosArray[posicion].MostrarDatos();
            else
                Console.WriteLine("No existe usuario");
        }




















        /*
        public void Menu()
        {
            int opcion = 0;
            do
            {
                Console.WriteLine("MENU: ( 0 -Salir)");
                Console.WriteLine(" 1 - Alta ejemplo");
                Console.WriteLine(" 2 - Mostrar todos");
                Console.WriteLine(" 3 - Mostrar uno");
                Console.WriteLine(" 4 - Eliminar uno");
                Console.WriteLine(" 5 - Modificar uno");
                string str_op = Console.ReadLine();
                if (int.TryParse(str_op, out opcion))
                {
                    switch (opcion)
                    {
                        case 1:
                            AltaEjemplo();
                            break;
                        case 2:
                            MostrarEjemplos();
                            break;
                        case 3:
                            MostrarUno();
                            break;
                        case 4:
                            EliminarUno();
                            break;
                        case 5:
                            ModificarUno();
                            break;
                        default:
                            Console.WriteLine("Introduzca opcion valida");
                            break;
                    }
                }
                else
                {
                    opcion = -1;
                    Console.WriteLine("Escribe bien el número");
                }
            } while (opcion != 0);
        }*/
    }
}
