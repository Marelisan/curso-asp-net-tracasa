﻿using System;
using System.Collections.Generic;
using System.Text;
using ModeloUsuarios;

namespace Singleton
{
    class GestorUsuarios
    {
            static GestorUsuarios instancia; //constate tipo GestorUsuarios

            private List<Usuario> usuarios; //Lista de usuarios

            public static GestorUsuarios Instancia //funcion para obtener el valor 
            {
                get
                {
                    if (instancia == null)
                        instancia = new GestorUsuarios();

                    return instancia;
                }
            }

            //Constructor
            private GestorUsuarios()
            {
                usuarios = new List<Usuario>();
            }

            public void Nuevo(Usuario usu)
            {
                usuarios.Add(usu);
            }
            public void Mostrar()
            {
                foreach (Usuario usu in usuarios)
                {
                    Console.WriteLine(usu);
                }
            }
        }
    }
}
