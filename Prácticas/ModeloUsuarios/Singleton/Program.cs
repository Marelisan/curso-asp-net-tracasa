﻿using System;

namespace Singleton
{
    class Program
    {
        static void Main(string[] args)
        {
            GestorUsuarios gestorUsu = GestorUsuarios.Instancia;

            gestorUsu.Nuevo();

        }
    }
}



class Modulo1
{
    public static void Main2(string[] argss)
    {
        GestorTextos gt = GestorTextos.Instancia;
        gt.Nuevo("AAAAA");
        gt.Nuevo("BBB");
        gt.Nuevo("CCCC");
        gt.Mostrar();
    }
}