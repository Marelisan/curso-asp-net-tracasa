class Modelo {
    constructor(usuarios) {
        this.usuarios = usuarios;
        window.localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
    }

    crearUsuario(nombre, edad, altura, activo) {
        let usuario = new Usuario(nombre, edad, altura, activo);
        this.usuarios.push(usuario);
    }

    listaUsuarios() {
        return this.usuarios;
    }

    modificarUsuario(usuario) {

    }

    eliminarUsuario(usuario) {

    }
}