/* JavaScript no tiene clases. Es un lenguaje prototípico. Basado en 
prototipos. */
window.onload = function() {
    let btnAnadir = document.getElementById("btn-anadir");
    btnAnadir.onclick = function() {
        let campoNombre = document.getElementById("nombre");
        campoNombre.value = campoNombre.value.toUpperCase();
        let nombre = campoNombre.value;
        let edad = parseInt(document.getElementById("edad").value);
        let altura = parseFloat(document.getElementById("altura").value);
        /*let sigDecada = edad + 10; */
        // Aquí copiamos la instancia del Object prototipo
        /*let usuario = new Object();
        usuario.nombre = nombre;
        usuario.edad = edad;
        usuario.altura = altura;
        */
       let usuario = {
           "nombre":nombre,
           "edad": edad,
           "altura": altura,
           "aficiones": aficiones
       }

     
        // Usamos notación JSON (JavsScript Object Notation)
        let aficiones =  {   // las llaves son como new Object();
            "cine": true,
            "leer": true,
            "musica": false
        };
        usuario.aficiones = aficiones;
        alert(textoAlertUsuario(usuario));
    };
    // Confiamos en que lo que llegue es un usuario
    function textoAlertUsuario(usu) {
        return `Yepa ${usu.nombre.toLowerCase()}  tienes ${usu.edad} y altura ${usu.altura} te gusta ${usu.aficiones.leer ? " leer " : ""} ${usu.aficiones.musica ? " musica " : ""} ${usu.aficiones.cine ? " cine " : ""}!`
    }
    document.getElementById("btn-ir").addEventListener("click", function() {
        window.location = document.getElementById("url").value;
    });
};